/** 
 * Project Name:sbs-common 
 * File Name:TestMain.java 
 * Package Name:com.demo.sbs 
 * Date:2019年1月15日上午11:01:55 
 * 
*/  
  
package com.demo.sbs;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import junit.framework.TestCase;


@SpringBootTest(classes = StartSpringBootMain.class)
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class TestMain {
	@Resource
	private StartSpringBootMain startSpringBootMain ;
	
//	@Test
//	public void testHome() {
//		TestCase.assertEquals(this.startSpringBootMain.home(), "www.demo.com");
//	}
}
 