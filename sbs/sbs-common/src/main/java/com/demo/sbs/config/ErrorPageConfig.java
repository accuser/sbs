package com.demo.sbs.config;

import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

/**
 * 错误页跳转配置
 * @author yanxin
 *
 */
@Configuration
public class ErrorPageConfig {
	
	/**
	 * 此方法使用JettyServletWebServerFactory,适配Jetty容器
	 * @return
	 */
//	@Bean
//	public ConfigurableServletWebServerFactory webServerFactory() {
//		JettyServletWebServerFactory factory = new JettyServletWebServerFactory();
//		ErrorPage page400 = new ErrorPage(HttpStatus.BAD_REQUEST, "/error-400.html");
//		ErrorPage page500 = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/error-500.html");
//		ErrorPage page404 = new ErrorPage(HttpStatus.NOT_FOUND, "/error-404.html");
//		factory.addErrorPages(page400,page500,page404);
//		return factory;
//	}
	
	/**
	 * 此方法使用TomcatServletWebServerFactory,适配Tomcat容器
	 * @return
	 */
	@Bean
	public ConfigurableServletWebServerFactory webTomcatServerFactory() {
		TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
		ErrorPage page400 = new ErrorPage(HttpStatus.BAD_REQUEST, "/error-400.html");
		ErrorPage page500 = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/error-500.html");
		ErrorPage page404 = new ErrorPage(HttpStatus.NOT_FOUND, "/error-404.html");
		factory.addErrorPages(page400,page500,page404);
		return factory;
	}
	
}
