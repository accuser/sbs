package com.demo.sbs.util.controller;

import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.annotation.Resource;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

/**
 * 抽象父类
 * 处理资源读取类的配置
 * 处理日期格式转换
 * @author yanxin
 *
 */
public abstract class AbstractBaseController {

		//配置资源读取类的配置
		@Resource
		private MessageSource messageSoure;	// 自动注入此资源对象

		public String getMessage(String key,String... args) {
			return this.messageSoure.getMessage(key, args,Locale.getDefault());
		}

		//日期格式转换
		@InitBinder
		public void initBinder(WebDataBinder binder) {	// 在本程序里面需要针对于日期格式进行处理
			// 首先建立一个可以将字符串转换为日期的工具程序类
			//sdf是从表单提交来的格式"2010-10-10",然后通过SimpleDateFormat进行日期格式转化
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd") ;
			// 明确的描述此时需要注册一个日期格式的转化处理程序类
			//binder.registerCustomEditor(java.time.LocalDate.class, new CustomDateEditor(sdf, true));
			binder.registerCustomEditor(java.util.Date.class, new CustomDateEditor(sdf, true));
		}
}
