/** 
 * Project Name:sbs-common 
 * File Name:HomeController.java 
 * Package Name:com.demo.sbs.controller 
 * Date:2019年1月15日上午11:42:33 
 * 
*/  
  
package com.demo.sbs.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/** 
 * ClassName:HomeController <br/> 
 * Function: 测试controller入口. <br/> 
 * Date:     2019年1月15日 上午11:42:33 <br/> 
 * @author   yanxmf 
 * @version   
 * @since    JDK 1.8 
 * @see       
 */
@RestController
public class HomeController {
	
	/**
	 * 
	 * home:首页跳转. <br/> 
	 * 
	 * @author yanxmf 
	 * @return http://localhost:8070/sbs
	 * @since JDK 1.8
	 */
	@RequestMapping("/")
	@ResponseBody
	public String home() {
		return "www.demo.com";
	}
	
	/**
	 * 
	 * echo:地址重写参数传递. <br/> 
	 * 
	 * @author yanxmf 
	 * @param msg
	 * @return http://localhost:8070/sbs/echo?msg=demo
	 * @since JDK 1.8
	 */
	@RequestMapping("/echo")
	@ResponseBody
	public String echo(String msg) {
		return "[Echo] " + msg;
	}
	
	/**
	 * 
	 * echoRest:Rest风格参数传递. <br/> 
	 * 
	 * @author yanxmf 
	 * @param msg
	 * @return http://localhost:8070/sbs/echorest/helloWorld
	 * @since JDK 1.8
	 */
	@RequestMapping(value = "/echorest/{message}",method = RequestMethod.GET)
	@ResponseBody
	public String echoRest(@PathVariable("message") String msg) {
		return "[Echo] " + msg;
	}
	
	/**
	 * 
	 * object:获取内置参数. <br/> 
	 * 
	 * @author yanxmf 
	 * @param request
	 * @param response
	 * @return http://localhost:8070/sbs/object
	 * @since JDK 1.8
	 */
	@RequestMapping("/object")
	@ResponseBody
	public Object object(HttpServletRequest request,HttpServletResponse response) {
		System.out.println("***客户端ip地址：" + request.getRemoteAddr());
		System.out.println("***获取客户端响应编码：" + response.getCharacterEncoding());
		System.out.println("***获取SessionID：" + request.getSession().getId());
		System.out.println("***获取真实路径：" + request.getServletContext().getRealPath("/upload/"));
		return null;
	}
}
 