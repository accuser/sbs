package com.demo.sbs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication	//启动springboot程序,而后自带子包扫描功能
public class StartSpringBootMain{

//	@RequestMapping("/")
//	@ResponseBody
//	public String home() {
//		return "www.demo.com";
//	}
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(StartSpringBootMain.class, args);
	}
} 