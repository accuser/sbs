/** 
 * Project Name:sbs-common 
 * File Name:MessageController.java 
 * Package Name:com.demo.sbs.controller 
 * Date:2019年1月16日上午9:41:57 
 * 
*/  
  
package com.demo.sbs.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.demo.sbs.util.controller.AbstractBaseController;

/** 
 * ClassName:MessageController <br/> 
 * Function: 测试thymeleaf跳转. <br/> 
 * Date:     2019年1月16日 上午9:41:57 <br/> 
 * @author   yanxmf 
 * @version   
 * @since    JDK 1.8 
 * @see       
 */
@Controller
public class MessageController extends AbstractBaseController {

	@RequestMapping(value="/show",method=RequestMethod.GET)
	public String show(String mid,Model model) {
		model.addAttribute("url","www.demo.com");
		model.addAttribute("mid", mid);
		return "message_show";
	}
}
 